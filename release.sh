#!/bin/bash
set -e

if [ ! -z "$(git status --porcelain)" ]; then
  echo Working directory must be clean!
  exit -1
fi

HASH=`git rev-parse --short HEAD`

IMAGE=ericlathrop/living_in_syndication:$HASH
docker build -t $IMAGE .
docker push $IMAGE
