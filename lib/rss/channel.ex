defmodule Rss.Channel do
  @moduledoc false
  use TypedStruct

  typedstruct do
    field(:atom_link, String.t())
    field(:category, String.t())
    field(:cloud, String.t())
    field(:copyright, String.t())
    field(:description, String.t(), enforce: true)
    field(:docs, String.t(), default: "https://www.rssboard.org/rss-specification")
    field(:generator, String.t(), default: "Living in Syndication")
    field(:image, Rss.Image.t())
    field(:items, [Rss.Item.t()])
    field(:language, String.t())
    field(:lastBuildDate, DateTime.t())
    field(:link, String.t(), enforce: true)
    field(:managingEditor, String.t())
    field(:pubDate, DateTime.t())
    field(:rating, String.t())
    field(:skipDays, integer())
    field(:skipHours, integer())
    field(:textInput, String.t())
    field(:title, String.t(), enforce: true)
    field(:ttl, String.t())
    field(:webMaster, String.t())
  end

  def to_xml(channel = %Rss.Channel{}) do
    {:channel, [],
     [
       {:title, [], [channel.title]},
       {:link, [], [channel.link]},
       {:description, [], [channel.description]},
       {:language, [], [channel.language]},
       {:copyright, [], [channel.copyright]},
       {:managingEditor, [], [channel.managingEditor]},
       {:webMaster, [], [channel.webMaster]},
       Rss.DateTime.date_tag(:pubDate, channel.pubDate),
       Rss.DateTime.date_tag(:lastBuildDate, channel.lastBuildDate),
       {:category, [], [channel.category]},
       {:generator, [], [channel.generator]},
       {:docs, [], [channel.docs]},
       # {:cloud, [], [channel.cloud]},
       {:ttl, [], [channel.ttl]},
       Rss.Image.to_xml(channel.image),
       {:rating, [], [channel.rating]},
       # {:textInput, [], [channel.textInput]},
       {:skipHours, [], [channel.skipHours]},
       {:skipDays, [], [channel.skipDays]},
       {:"atom:link", [{:href, channel.atom_link}, {:rel, "self"}], []}
     ]
     |> Enum.concat(Enum.map(channel.items, &Rss.Item.to_xml/1))
     |> Rss.Xml.cleanup_content()}
  end
end
