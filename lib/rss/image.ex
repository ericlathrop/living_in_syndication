defmodule Rss.Image do
  @moduledoc false
  use TypedStruct

  import Meeseeks.CSS, only: [css: 1]

  typedstruct do
    field(:description, String.t())
    field(:height, integer())
    field(:link, String.t(), enforce: true)
    field(:title, String.t(), enforce: true)
    field(:url, String.t(), enforce: true)
    field(:width, integer())
  end

  def to_xml(%Rss.Image{
        url: url,
        title: title,
        link: link,
        width: width,
        height: height,
        description: description
      }) do
    {:image, [],
     [
       {:url, [], [url]},
       {:title, [], [title]},
       {:link, [], [link]},
       {:width, [], [to_string(width)]},
       {:height, [], [to_string(height)]},
       {:description, [], [description]}
     ]
     |> Rss.Xml.cleanup_content()}
  end

  @max_width 144
  @max_height 400
  @max_aspect_ratio @max_width / @max_height
  def constrain_dimensions(image = %Rss.Image{width: nil, height: nil}), do: image

  def constrain_dimensions(image = %Rss.Image{width: width, height: height}) do
    aspect_ratio = width / height

    cond do
      width < @max_width && height < @max_height ->
        image

      aspect_ratio >= @max_aspect_ratio ->
        scale_factor = @max_width / width

        image
        |> Map.put(:width, @max_width)
        |> Map.put(:height, floor(height * scale_factor))

      true ->
        scale_factor = @max_height / height

        image
        |> Map.put(:width, floor(width * scale_factor))
        |> Map.put(:height, @max_height)
    end
  end

  def from_link_rel_icon(document, title, link) do
    {width, height, url} =
      Meeseeks.all(document, css("link[rel=\"icon\"]"))
      |> Enum.map(&extract_icon_data/1)
      |> Enum.sort()
      |> List.last()

    %Rss.Image{
      url: url,
      title: title,
      link: link,
      width: width,
      height: height
    }
    |> Rss.Image.constrain_dimensions()
  end

  defp extract_icon_data(queryable) do
    href = Meeseeks.attr(queryable, "href")
    sizes = Meeseeks.attr(queryable, "sizes") || "1x1"

    [width, height] =
      String.split(sizes, "x")
      |> Enum.map(&String.to_integer/1)

    {width, height, href}
  end
end
