defmodule Rss.Enclosure do
  @moduledoc false
  use TypedStruct

  typedstruct do
    field(:length, integer(), enforce: true)
    field(:type, String.t(), enforce: true)
    field(:url, String.t(), enforce: true)
  end

  def from_url(url) do
    image_headers = HTTP.Client.get_headers!(url)

    %Rss.Enclosure{
      url: url,
      length: HTTP.Client.get_header(image_headers, "content-length"),
      type: HTTP.Client.get_header(image_headers, "content-type")
    }
  end

  def to_xml(nil) do
    {:enclosure, [], []}
  end

  def to_xml(%Rss.Enclosure{url: url, length: length, type: type}) do
    {:enclosure, [{:url, url}, {:length, length}, {:type, type}], []}
  end
end
