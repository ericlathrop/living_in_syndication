defmodule LivingInSyndication.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    port =
      System.get_env("PORT", "4001")
      |> String.to_integer()

    children = [
      {HTTP.Cache, []},
      {Plug.Cowboy, scheme: :http, plug: LivingInSyndication.Router, options: [port: port]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LivingInSyndication.Supervisor]
    ret = Supervisor.start_link(children, opts)
    IO.puts("Listening on port #{port}")
    ret
  end
end
