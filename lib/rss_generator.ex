defmodule RssGenerator do
  @moduledoc false

  @callback get_metadata() :: RssGenerator.Metadata.t()
  @callback get_channel(map()) :: {:ok, Rss.Channel.t()} | {:error, any()}

  defmacro __using__(metadata) do
    quote do
      @behaviour RssGenerator

      defimpl RssGenerator.ReflectionTag, for: __MODULE__ do
        def _info(x), do: x
      end

      @impl RssGenerator
      def get_metadata, do: unquote(metadata)
    end
  end

  def get_implementors do
    {:consolidated, modules} = RssGenerator.ReflectionTag.__protocol__(:impls)

    modules
    |> Enum.map(&get_implementor/1)
  end

  defp get_implementor(module) do
    metadata = module.get_metadata()
    {module, metadata}
  end
end
