defmodule HTML.OpenGraph do
  @moduledoc false

  import Meeseeks.CSS, only: [css: 1]

  def extract_from_html(document = %Meeseeks.Document{}) do
    document
    |> Meeseeks.all(css("meta[property^=\"og:\"]"))
    |> Enum.map(fn tag ->
      {Meeseeks.attr(tag, "property"), Meeseeks.attr(tag, "content")}
    end)
    |> Map.new()
  end
end
