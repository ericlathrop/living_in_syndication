defmodule HTML.LinkedData do
  @moduledoc false

  import Meeseeks.CSS, only: [css: 1]

  def extract_from_html(document = %Meeseeks.Document{}) do
    document
    |> Meeseeks.one(css("script[type=\"application/ld+json\"]"))
    |> Meeseeks.data()
    |> Jason.decode!()
  end

  def extract_graph(%{"@context" => "https://schema.org", "@graph" => graph}) do
    graph
    |> Enum.map(fn %{"@id" => id} = node -> {id, node} end)
    |> Map.new()
  end

  def graph_find_item_of_type(graph, type) when is_map(graph) and is_binary(type) do
    {_key, item} =
      graph
      |> Enum.find(fn
        {_key, %{"@type" => ^type}} -> true
        _ -> false
      end)

    item
  end
end
