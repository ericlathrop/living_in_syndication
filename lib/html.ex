defmodule HTML do
  @moduledoc false

  import Meeseeks.CSS, only: [css: 1]

  def title(document) do
    document
    |> Meeseeks.one(css("title"))
    |> Meeseeks.text()
  end

  def meta_description(document) do
    Meeseeks.one(document, css("meta[name=\"description\"]"))
    |> Meeseeks.attr("content")
  end

  def meta_refresh(document) do
    refresh =
      document
      |> Meeseeks.one(css("meta[http-equiv=\"refresh\"]"))
      |> Meeseeks.attr("content")

    [_full_match, delay_seconds, url] = Regex.run(~r/(\d+);\s*url=(.*)/i, refresh)
    {String.to_integer(delay_seconds), url}
  end
end
