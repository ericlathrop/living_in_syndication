defmodule RssGenerator.Metadata do
  @moduledoc false

  alias RssGenerator.Metadata

  @enforce_keys [:name, :url]
  defstruct [:name, :url, params: []]

  @type t() :: %Metadata{
          name: String.t(),
          url: String.t(),
          params: [RssGenerator.Param.t()]
        }

  def host(%Metadata{url: url}) do
    %URI{host: host} = URI.parse(url)
    host
  end

  def rss_path(metadata = %Metadata{}) do
    "/rss/" <> host(metadata)
  end
end
