defmodule Feeds.AbcbirdsOrg do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "American Bird Conservancy",
        url: "https://abcbirds.org"
      }

  import Meeseeks.CSS, only: [css: 1]

  @news_listing_url "https://abcbirds.org/results/news/"

  @impl RssGenerator
  def get_channel(_params) do
    document = get_document(@news_listing_url)

    graph =
      document
      |> HTML.LinkedData.extract_from_html()
      |> HTML.LinkedData.extract_graph()

    web_page = HTML.LinkedData.graph_find_item_of_type(graph, "WebPage")
    organization = HTML.LinkedData.graph_find_item_of_type(graph, "Organization")

    {:ok, date, _offset} = DateTime.from_iso8601(web_page["dateModified"])

    items =
      document
      |> Meeseeks.all(css(".news-room > .row"))
      |> Enum.map(&extract_item/1)

    {:ok,
     %Rss.Channel{
       title: web_page["name"],
       link: @news_listing_url,
       description: web_page["description"],
       language: web_page["inLanguage"],
       pubDate: date,
       lastBuildDate: date,
       image:
         %Rss.Image{
           url: organization["logo"]["url"],
           title: web_page["name"],
           link: @news_listing_url,
           width: organization["logo"]["width"],
           height: organization["logo"]["height"],
           description: organization["logo"]["caption"]
         }
         |> Rss.Image.constrain_dimensions(),
       items: items
     }}
  end

  defp extract_item(queryable) do
    link = Meeseeks.one(queryable, css("h3 a"))
    title = Meeseeks.text(link)
    url = Meeseeks.attr(link, "href")

    description =
      Meeseeks.one(queryable, css(".summary"))
      |> Meeseeks.text()

    pub_date =
      Meeseeks.one(queryable, css(".posted_date"))
      |> Meeseeks.text()
      |> Timex.parse!("{Mfull} {D}, {YYYY}")
      |> Timex.to_datetime("America/New_York")

    image =
      Meeseeks.one(queryable, css(".image img"))
      |> Meeseeks.attr("src")

    description_with_image = """
      <p>#{description}</p>
      <p><img src="#{image}" alt="#{title}" /></p>
    """

    %Rss.Item{
      title: title,
      link: url,
      description: description_with_image,
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_document(url) do
    url
    |> HTTP.Client.get_body!()
    |> Meeseeks.parse()
  end
end
