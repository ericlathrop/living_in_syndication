defmodule Feeds.GitHubComReleases do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "GitHub Releases",
        url: "https://www.github.com",
        params: [
          %RssGenerator.Param{
            name: "owner",
            type: "text",
            description: "The GitHub repo owner (user/organization)"
          },
          %RssGenerator.Param{
            name: "repo",
            type: "text",
            description: "The GitHub repository name"
          }
        ]
      }

  @impl RssGenerator
  def get_channel(%{"owner" => owner, "repo" => repo}) do
    {:error, {:redirect, "https://github.com/#{owner}/#{repo}/releases.atom"}}
  end
end
