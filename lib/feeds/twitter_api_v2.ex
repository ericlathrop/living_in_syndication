defmodule Feeds.TwitterAPIv2 do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "Twitter API v2",
        url: "https://api.twitter.com/2/",
        params: [
          %RssGenerator.Param{
            name: "bearer_token",
            type: "text",
            description:
              "The OAuth 2.0 Bearer Token you generated for your Twitter developer App."
          },
          %RssGenerator.Param{
            name: "query",
            type: "text",
            description: "The twitter search query, for example: \"from:josevalim\""
          }
        ]
      }

  @tweets_search_url "https://api.twitter.com/2/tweets/search/recent"
  @twitter_base_url "https://twitter.com/"

  @doc """
  `bearer_token` is required for authorization.
  `query` is required and support all `Twiiter` API v2 queries,for example: "from:josevalim".
  `params` optionally passes extra parameters in order to change query parameters like `max_results`.
  `comment_params` works as same as `params` except that they are used for comments queries.

  Examples:

      iex> Feeds.TwitterAPIv2.get_channel(%{"bearer_token" => "SECRET", "comment_params" => %{max_results: 100}, "params" => %{max_results: 100}, "query" => "from:josevalim"})
  """
  @impl RssGenerator
  def get_channel(arg = %{"bearer_token" => token, "query" => query}) do
    params = arg["params"] || %{}

    param_query =
      params
      |> Map.merge(%{
        "expansions" => "attachments.media_keys,author_id",
        "media.fields" => "url",
        "query" => query,
        "tweet.fields" => "created_at,lang,referenced_tweets,source",
        "user.fields" => "description,profile_image_url,url"
      })
      |> URI.encode_query()

    url = @tweets_search_url |> URI.parse() |> Map.put(:query, param_query) |> URI.to_string()
    {:ok, response} = get_document(url, token)
    %{"data" => data} = response
    %{"users" => [user]} = response["includes"]
    %{"id" => _id, "name" => user_name, "profile_image_url" => img, "username" => mention} = user
    media = response |> get_in(~w/includes media/) |> prepare_media()
    title = "#{user_name} (@#{mention}) / Twitter"
    [item | _] = items = Enum.map(data, &extract_item(&1, arg, token, user_name, mention, media))
    pub_date = item.pubDate

    {:ok,
     %Rss.Channel{
       atom_link: url,
       description: user["description"],
       image: img,
       items: items,
       language: "en-us",
       lastBuildDate: pub_date,
       link: user["url"],
       pubDate: pub_date,
       title: title
     }}
  end

  defp prepare_media(nil), do: %{}

  defp prepare_media(media) when is_list(media) do
    Enum.reduce(media, %{}, &prepare_media/2)
  end

  defp prepare_media(media, acc) do
    Map.put(acc, media["media_key"], %{type: media["type"], url: media["url"]})
  end

  defp extract_item(data, arg, bearer_token, name, mention, media) do
    url = Path.join([@twitter_base_url, mention, "status", data["id"]])
    created_at = NaiveDateTime.from_iso8601!(data["created_at"])
    date_short = Timex.format!(created_at, "%-d %b", :strftime)
    title = "#{name} @#{mention} · #{date_short}"
    pub_date = Timex.to_datetime(created_at, "America/New_York")
    attachments = data |> get_in(~w[attachments media_keys]) |> attachments_html(media)
    description = "<p>" <> data["text"] <> "</p>" <> attachments
    id = data["id"]

    %Rss.Item{
      author: name,
      comments: get_comments(id, arg, bearer_token),
      description: description,
      guid: id,
      link: url,
      pubDate: pub_date,
      source: data["source"],
      title: title
    }
  end

  defp attachments_html(nil, _media), do: ""

  defp attachments_html(attachments, media) do
    "<p>" <> Enum.map_join(attachments, "</p><p>", &attachment_html(&1, media)) <> "</p>"
  end

  defp attachment_html(url, image) when image in ~w[animated_gif photo] do
    ~s(<img src="#{url}" />)
  end

  defp attachment_html(url, "video"), do: ~s(<video src="#{url}" />)

  defp attachment_html(id, media) do
    %{^id => %{type: type, url: url}} = media
    attachment_html(url, type)
  end

  defp get_comments(conversation_id, arg, bearer_token) do
    params = arg["comment_params"] || %{}

    param_query =
      params
      |> Map.merge(%{"query" => "conversation_id:#{conversation_id}"})
      |> URI.encode_query()

    url = @tweets_search_url |> URI.parse() |> Map.put(:query, param_query) |> URI.to_string()
    {:ok, response} = get_document(url, bearer_token)
    Enum.map(response["data"] || [], & &1["text"])
  end

  defp get_document(url, bearer_token) do
    url
    |> HTTP.Client.get_body!([{"Authorization", "Bearer #{bearer_token}"}])
    |> Jason.decode()
  end
end
