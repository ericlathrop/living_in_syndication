defmodule Feeds.InstagramCom do
  @moduledoc false

  @cache_hours 24

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "Instagram",
        url: "https://www.instagram.com",
        params: [
          %RssGenerator.Param{
            name: "user",
            type: "text",
            description: "The Instagram user to fetch posts for"
          }
        ]
      }

  import Meeseeks.CSS, only: [css: 1]

  @impl RssGenerator
  def get_channel(%{"user" => user}) do
    with {:ok, user_graphql} <-
           get_profile_page_scripts(user)
           |> get_user_graphql() do
      {full_name, profile_pic_url} = get_user_profile(user_graphql)

      title = "#{full_name} (@#{user}) - Instagram"
      profile_url = "https://www.instagram.com/#{user}/"

      items = get_posts(user_graphql)

      pub_date =
        Enum.map(items, & &1.pubDate)
        |> Enum.max(&>=/2, fn -> DateTime.utc_now() end)

      {:ok,
       %Rss.Channel{
         title: title,
         link: profile_url,
         description: title,
         language: "en-us",
         pubDate: pub_date,
         lastBuildDate: pub_date,
         image:
           %Rss.Image{
             url: profile_pic_url,
             title: title,
             link: profile_url,
             width: 150,
             height: 150
           }
           |> Rss.Image.constrain_dimensions(),
         items: items,
         ttl: to_string(@cache_hours * 60)
       }}
    end
  end

  def get_posts(%{"edge_owner_to_timeline_media" => %{"edges" => edges}}) do
    edges
    |> Enum.map(&edge_to_item/1)
  end

  def edge_to_item(
        edge = %{
          "node" => %{"shortcode" => shortcode, "taken_at_timestamp" => taken_at_timestamp}
        }
      ) do
    url = "https://www.instagram.com/p/#{shortcode}/"

    caption = get_caption(edge)
    [title | _] = String.split(caption, "\n")
    pub_date = DateTime.from_unix!(taken_at_timestamp)

    description =
      get_description(edge) <>
        """
        <p>
          #{String.replace(caption, "\n", "<br>")}
        </p>
        """

    %Rss.Item{
      title: title,
      link: url,
      description: description,
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_description(%{
         "node" => %{
           "__typename" => "GraphSidecar",
           "edge_sidecar_to_children" => %{"edges" => edges}
         }
       }) do
    edges
    |> Enum.map(&get_description/1)
    |> Enum.join("\n")
  end

  defp get_description(
         edge = %{
           "node" => %{"__typename" => "GraphImage", "display_url" => display_url}
         }
       ) do
    """
    <p>
      <img src="#{display_url}" alt="#{get_caption(edge)}" />
    </p>
    """
  end

  defp get_description(
         edge = %{
           "node" => %{
             "__typename" => "GraphVideo",
             "display_url" => display_url,
             "video_url" => video_url
           }
         }
       ) do
    """
    <p>
      <video controls>
        <source src="#{video_url}" poster="#{display_url}" type="video/mp4">
        <img src="#{video_url}" alt="#{get_caption(edge)}">
      </video>
    </p>
    """
  end

  defp get_caption(%{
         "node" => %{"edge_media_to_caption" => %{"edges" => [%{"node" => %{"text" => text}}]}}
       }),
       do: text

  defp get_caption(_), do: "(no text)"

  def get_user_profile(%{"full_name" => full_name, "profile_pic_url" => profile_pic_url}) do
    {full_name, profile_pic_url}
  end

  defp get_profile_page_scripts(username) when is_binary(username) do
    headers = [
      {"user-agent", Http.UserAgents.random_desktop()}
    ]

    HTTP.Cache.request(:get, "https://www.instagram.com/#{username}/", headers,
      hours: -@cache_hours
    )
    |> get_profile_page_scripts()
  end

  defp get_profile_page_scripts({:ok, %Mojito.Response{status_code: 200, body: body}}) do
    scripts =
      body
      |> Meeseeks.all(css("script"))
      |> Enum.map(&Meeseeks.data(&1))

    {:ok, scripts}
  end

  defp get_profile_page_scripts(
         {:error,
          %Mojito.Response{
            status_code: 302
          }}
       ) do
    {:error, :rate_limited}
  end

  defp get_user_graphql({:error, reason}), do: {:error, reason}

  defp get_user_graphql({:ok, scripts}) do
    %{
      "entry_data" => %{
        "ProfilePage" => [
          %{"graphql" => %{"user" => graphql}}
        ]
      }
    } =
      Enum.map(scripts, &Regex.run(~r/^\s*window._sharedData\s*=\s*(.+);$/m, &1))
      |> Enum.find_value(fn
        nil -> false
        [_, json] -> json
      end)
      |> Jason.decode!()

    {:ok, graphql}
  end
end
