defmodule Feeds.ApnewsCom do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "AP Top News",
        url: "https://apnews.com"
      }

  import Meeseeks.CSS, only: [css: 1]

  @news_listing_url "https://apnews.com/hub/ap-top-news"

  @impl RssGenerator
  def get_channel(_params) do
    document = get_document(@news_listing_url)
    open_graph = HTML.OpenGraph.extract_from_html(document)
    items = extract_items(document)
    date = Enum.map(items, & &1.pubDate) |> Enum.max()

    image =
      document
      |> Meeseeks.one(css(~s/link[rel="apple-touch-icon"][sizes="167x167"]/))
      |> Meeseeks.attr("href")

    {:ok,
     %Rss.Channel{
       title: open_graph["og:site_name"],
       link: @news_listing_url,
       description: open_graph["og:site_name"],
       language: "en-us",
       pubDate: date,
       lastBuildDate: date,
       image:
         %Rss.Image{
           url: "https://apnews.com" <> image,
           title: open_graph["og:site_name"],
           link: @news_listing_url,
           width: 167,
           height: 167
         }
         |> Rss.Image.constrain_dimensions(),
       items: items
     }}
  end

  def extract_items(document) do
    json =
      Meeseeks.one(document, css("body > script[src=\"\"]"))
      |> Meeseeks.data()
      |> String.replace(~r/.*window\['titanium-state'\]\s*=/, "")
      |> String.replace(~r/window\['titanium.*/, "")
      |> Jason.decode!()

    json["hub"]["data"]["/ap-top-news"]["cards"]
    |> Enum.map(&card_to_item/1)
    |> Enum.reject(&(&1 == nil))
  end

  def card_to_item(%{
        "cardType" => "Hub Peek"
      }),
      do: nil

  def card_to_item(%{
        "cardType" => "Notification"
      }),
      do: nil

  def card_to_item(%{
        "contents" => [
          %{
            "canonicalUrl" => canonical_url,
            "headline" => headline,
            "leadPhotoId" => lead_photo_id,
            "shortId" => guid,
            "storyHTML" => description,
            "updated" => updated
          }
        ]
      }) do
    url = "https://apnews.com/article/#{canonical_url}-#{guid}"

    pub_date =
      updated
      |> Timex.parse!("{YYYY}-{M}-{D} {h24}:{m}:{s}")
      |> Timex.to_datetime("Etc/UTC")

    # Getting images is too slow, make an educated guess instead
    # {image_url, image_caption} = get_photo(lead_photo_id)
    image_url = "https://storage.googleapis.com/afs-prod/media/#{lead_photo_id}/400.jpeg"
    image_caption = headline

    description_with_image = """
    <p><img src="#{image_url}" alt="#{image_caption}" /></p>
    #{description}
    """

    %Rss.Item{
      title: headline,
      link: url,
      description: description_with_image,
      guid: url,
      pubDate: pub_date
    }
  end

  # def get_photo(photo_id) do
  #   %{
  #     "flattenedCaption" => caption,
  #     "imageFileExtension" => extension,
  #     "imageRenderedSizes" => [size | _]
  #   } =
  #     HTTP.Client.get_body!("https://afs-prod.appspot.com/api/v2/medium/" <> photo_id)
  #     |> Jason.decode!()

  #   url = "https://storage.googleapis.com/afs-prod/media/#{photo_id}/#{size}#{extension}"
  #   {url, caption}
  # end

  defp get_document(url) do
    url
    |> HTTP.Client.get_body!()
    |> Meeseeks.parse()
  end
end
