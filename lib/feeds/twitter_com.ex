defmodule Feeds.TwitterCom do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "Twitter",
        url: "https://twitter.com",
        params: [
          %RssGenerator.Param{
            name: "include_retweets",
            type: "checkbox",
            description: "Should feed include retweets?"
          },
          %RssGenerator.Param{
            name: "user",
            type: "text",
            description: "The Twitter user to fetch posts for"
          }
        ]
      }

  require Logger

  import Meeseeks.CSS, only: [css: 1]

  @impl RssGenerator
  def get_channel(params = %{"user" => user}) do
    %{
      "data" => %{
        "user" => %{
          "rest_id" => user_id,
          "legacy" => %{
            "description" => description,
            "name" => name,
            "profile_image_url_https" => profile_pic_url
          }
        }
      }
    } = get_user_by_screen_name(user)

    include_retweets = Map.get(params, "include_retweets") == "on"

    title = "#{name} (@#{user}) - Twitter"
    profile_url = "https://twitter.com/#{user}/"

    items =
      get_items(user_id, include_retweets)
      |> Enum.reject(&is_nil?/1)

    pub_date = Enum.map(items, & &1.pubDate) |> max_date()

    {:ok,
     %Rss.Channel{
       title: title,
       link: profile_url,
       description: description,
       language: "en-us",
       pubDate: pub_date,
       lastBuildDate: pub_date,
       image:
         %Rss.Image{
           url: profile_pic_url,
           title: title,
           link: profile_url,
           width: 48,
           height: 48
         }
         |> Rss.Image.constrain_dimensions(),
       items: items
     }}
  end

  defp max_date([]), do: DateTime.utc_now()
  defp max_date(dates) when is_list(dates), do: Enum.max(dates)

  defp is_nil?(nil), do: true
  defp is_nil?(_), do: false

  def get_user_by_screen_name(user) do
    variables =
      %{"screen_name" => user, "withHighlightedLabel" => true}
      |> Jason.encode!()

    query_string =
      %{"variables" => variables}
      |> URI.encode_query()

    get_json(
      "https://api.twitter.com/graphql/jMaTS-_Ea8vh9rpKggJbCQ/UserByScreenName?#{query_string}"
    )
  end

  def get_items(user_id, include_retweets) do
    query_string =
      %{
        "include_profile_interstitial_type" => 1,
        "include_blocking" => 1,
        "include_blocked_by" => 1,
        "include_followed_by" => 1,
        "include_want_retweets" => 1,
        "include_mute_edge" => 1,
        "include_can_dm" => 1,
        "include_can_media_tag" => 1,
        "skip_status" => 1,
        "cards_platform" => "Web-12",
        "include_cards" => 1,
        "include_ext_alt_text" => true,
        "include_quote_count" => true,
        "include_reply_count" => 1,
        "tweet_mode" => "extended",
        "include_entities" => true,
        "include_user_entities" => true,
        "include_ext_media_color" => true,
        "include_ext_media_availability" => true,
        "send_error_codes" => true,
        "simple_quoted_tweet" => true,
        "include_tweet_replies" => false,
        "count" => 20,
        "userId" => user_id,
        "ext" => "mediaStats,highlightedLabel"
      }
      |> URI.encode_query()

    %{
      "globalObjects" => %{
        "tweets" => tweets,
        "users" => users
      },
      "timeline" => %{"instructions" => instructions}
    } = get_json("https://api.twitter.com/2/timeline/profile/#{user_id}.json?#{query_string}")

    # get_json("https://api.twitter.com/2/timeline/media/#{user_id}.json?#{query_string}")

    instructions
    |> Enum.flat_map(&timeline_instruction_to_tweets(&1, tweets, users, include_retweets))
    |> Enum.reject(&is_nil/1)
  end

  defp timeline_instruction_to_tweets(
         %{"addEntries" => %{"entries" => entries}},
         tweets,
         users,
         include_retweets
       ) do
    entries
    |> Enum.map(fn
      %{
        "content" => %{
          "item" => %{
            "content" => %{"tweet" => %{"id" => id, "displayType" => _display_type}}
          }
        }
      } ->
        tweet_to_item(tweets[id], tweets, users, include_retweets)

      %{
        "content" => %{
          "timelineModule" => %{}
        }
      } ->
        nil

      %{
        "content" => %{
          "operation" => %{
            "cursor" => %{"cursorType" => _cursor_type, "value" => _value}
          }
        }
      } ->
        nil
    end)
  end

  defp timeline_instruction_to_tweets(_, _tweets, _users, _include_retweets), do: []

  defp tweet_to_item(%{"retweeted_status_id_str" => _retweet_id}, _tweets, _users, false), do: nil
  defp tweet_to_item(nil, _tweets, _users, _include_retweets), do: nil

  defp tweet_to_item(tweet = %{"full_text" => full_text}, tweets, users, _include_retweets) do
    url = tweet_url(tweet, users)

    %Rss.Item{
      title: full_text,
      link: url,
      description: tweet_description_html(tweet, tweets, users),
      guid: url,
      pubDate: tweet_date(tweet)
    }
  end

  # handle retweets
  defp tweet_description_html(
         %{
           "user_id_str" => user_id_str,
           "retweeted_status_id_str" => retweet_id
         },
         tweets,
         users
       ) do
    %{"name" => name, "screen_name" => screen_name} = users[user_id_str]

    """
    <p>
      <a href="https://twitter.com/#{screen_name}">
        <strong>#{name} Retweeted</strong>
       </a>
    </p>
    #{tweet_description_html(tweets[retweet_id], tweets, users)}
    """
  end

  defp tweet_description_html(
         tweet = %{
           "full_text" => full_text,
           "user_id_str" => user_id_str
         },
         tweets,
         users
       ) do
    %{"name" => name, "profile_image_url_https" => profile_pic_url, "screen_name" => screen_name} =
      users[user_id_str]

    """
    <p>
      <a href="https://twitter.com/#{screen_name}">
        <img src="#{profile_pic_url}" alt="Profile picture for #{name} (@#{screen_name})" />
      </a>
      <a href="https://twitter.com/#{screen_name}">
        <strong>#{name}</strong>
      </a>
      <em>
        (
          <a href="https://twitter.com/#{screen_name}">
            @#{screen_name}
          </a>
        )
      </em>

      <a href="#{tweet_url(tweet, users)}">
        #{Timex.format!(tweet_date(tweet), "{Mshort} {D}")}
      </a>
    </p>
    <p>#{full_text}</p>
    #{card_html(tweet)}
    #{media_html(tweet)}
    #{quoted_tweet_html(tweet, tweets, users)}
    """
  end

  defp tweet_url(
         %{
           "id_str" => tweet_id,
           "user_id_str" => user_id_str
         },
         users
       ) do
    %{"screen_name" => screen_name} = users[user_id_str]
    "https://twitter.com/#{screen_name}/status/#{tweet_id}"
  end

  defp tweet_date(%{
         "created_at" => created_at
       }) do
    Timex.parse!(created_at, "{WDfull} {Mshort} {D} {h24}:{m}:{s} {Z} {YYYY}")
  end

  defp quoted_tweet_html(
         %{
           "quoted_status_id_str" => tweet_id,
           "quoted_status_permalink" => %{
             "expanded" => tweet_url
           }
         },
         tweets,
         users
       ) do
    case tweets[tweet_id] do
      # probably a 2nd level quote
      nil ->
        ""

      quoted_tweet ->
        """
        <blockquote cite="#{tweet_url}">
          #{tweet_description_html(quoted_tweet, tweets, users)}
        </blockquote>
        """
    end
  end

  # no quoted tweet
  defp quoted_tweet_html(_tweet, _tweets, _users), do: ""

  defp card_html(
         card = %{
           "card" => %{
             "binding_values" => %{
               "card_url" => %{"string_value" => card_url},
               "domain" => %{"string_value" => domain},
               "player_image_original" => %{
                 "image_value" => %{"url" => image_url}
               },
               "player_url" => %{"string_value" => player_url},
               "player_width" => %{"string_value" => player_width},
               "player_height" => %{"string_value" => player_height},
               "title" => %{"string_value" => title}
             },
             "name" => "player"
           }
         }
       ) do
    description_html =
      case card do
        %{
          "binding_values" => %{
            "description" => %{"string_value" => description}
          }
        } ->
          "<p>#{description}</p>"

        _ ->
          ""
      end

    """
    <blockquote cite="#{card_url}">
      <iframe width="#{player_width}" height="#{player_height}" src="#{player_url}">
        <img src="#{image_url}" alt="#{title}" />
      </iframe>
      <p>
        <a href="#{card_url}">
          <img src="#{image_url}" alt="#{title}" />
         </a>
      </p>
      <p>
        <a href="#{card_url}">
          <strong>#{title}</strong>
        </a>
      </p>
      #{description_html}
      <p>
        <a href="#{card_url}">
          <svg width="14" height="14" viewBox="0 0 24 24" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr"><g><path d="M11.96 14.945c-.067 0-.136-.01-.203-.027-1.13-.318-2.097-.986-2.795-1.932-.832-1.125-1.176-2.508-.968-3.893s.942-2.605 2.068-3.438l3.53-2.608c2.322-1.716 5.61-1.224 7.33 1.1.83 1.127 1.175 2.51.967 3.895s-.943 2.605-2.07 3.438l-1.48 1.094c-.333.246-.804.175-1.05-.158-.246-.334-.176-.804.158-1.05l1.48-1.095c.803-.592 1.327-1.463 1.476-2.45.148-.988-.098-1.975-.69-2.778-1.225-1.656-3.572-2.01-5.23-.784l-3.53 2.608c-.802.593-1.326 1.464-1.475 2.45-.15.99.097 1.975.69 2.778.498.675 1.187 1.15 1.992 1.377.4.114.633.528.52.928-.092.33-.394.547-.722.547z"></path><path d="M7.27 22.054c-1.61 0-3.197-.735-4.225-2.125-.832-1.127-1.176-2.51-.968-3.894s.943-2.605 2.07-3.438l1.478-1.094c.334-.245.805-.175 1.05.158s.177.804-.157 1.05l-1.48 1.095c-.803.593-1.326 1.464-1.475 2.45-.148.99.097 1.975.69 2.778 1.225 1.657 3.57 2.01 5.23.785l3.528-2.608c1.658-1.225 2.01-3.57.785-5.23-.498-.674-1.187-1.15-1.992-1.376-.4-.113-.633-.527-.52-.927.112-.4.528-.63.926-.522 1.13.318 2.096.986 2.794 1.932 1.717 2.324 1.224 5.612-1.1 7.33l-3.53 2.608c-.933.693-2.023 1.026-3.105 1.026z"></path></g></svg>
          #{domain}
         </a>
       </p>
    </blockquote>
    """
  end

  defp card_html(%{
         "card" => %{
           "binding_values" => %{
             "choice1_count" => %{"string_value" => choice1_count},
             "choice1_label" => %{"string_value" => choice1_label},
             "choice2_count" => %{"string_value" => choice2_count},
             "choice2_label" => %{"string_value" => choice2_label},
             "choice3_count" => %{"string_value" => choice3_count},
             "choice3_label" => %{"string_value" => choice3_label},
             "choice4_count" => %{"string_value" => choice4_count},
             "choice4_label" => %{"string_value" => choice4_label}
           },
           "name" => "poll4choice_text_only"
         }
       }) do
    options = [
      {String.to_integer(choice1_count), choice1_label},
      {String.to_integer(choice2_count), choice2_label},
      {String.to_integer(choice3_count), choice3_label},
      {String.to_integer(choice4_count), choice4_label}
    ]

    total_count = Enum.reduce(options, 0, fn {count, _label}, acc -> count + acc end)

    options_html =
      options
      |> Enum.map(fn {count, label} -> poll_option_html(label, count, total_count) end)
      |> Enum.join("<br>\n")

    """
    <p>
      #{options_html}
      <br>
      #{total_count} votes
    </p>
    """
  end

  defp card_html(
         card = %{
           "card" => %{
             "binding_values" => %{
               "card_url" => %{"string_value" => card_url},
               "domain" => %{"string_value" => domain},
               "title" => %{"string_value" => title}
             },
             "name" => "summary" <> _
           }
         }
       ) do
    description_html =
      case card do
        %{
          "binding_values" => %{
            "description" => %{"string_value" => description}
          }
        } ->
          "<p>#{description}</p>"

        _ ->
          ""
      end

    """
    <blockquote cite="#{card_url}">
      #{card_image_html(card_url, card_image_url(card), title)}
      <p>
        <a href="#{card_url}">
          <strong>#{title}</strong>
        </a>
      </p>
      #{description_html}
      <p>
        <a href="#{card_url}">
          <svg width="14" height="14" viewBox="0 0 24 24" class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr"><g><path d="M11.96 14.945c-.067 0-.136-.01-.203-.027-1.13-.318-2.097-.986-2.795-1.932-.832-1.125-1.176-2.508-.968-3.893s.942-2.605 2.068-3.438l3.53-2.608c2.322-1.716 5.61-1.224 7.33 1.1.83 1.127 1.175 2.51.967 3.895s-.943 2.605-2.07 3.438l-1.48 1.094c-.333.246-.804.175-1.05-.158-.246-.334-.176-.804.158-1.05l1.48-1.095c.803-.592 1.327-1.463 1.476-2.45.148-.988-.098-1.975-.69-2.778-1.225-1.656-3.572-2.01-5.23-.784l-3.53 2.608c-.802.593-1.326 1.464-1.475 2.45-.15.99.097 1.975.69 2.778.498.675 1.187 1.15 1.992 1.377.4.114.633.528.52.928-.092.33-.394.547-.722.547z"></path><path d="M7.27 22.054c-1.61 0-3.197-.735-4.225-2.125-.832-1.127-1.176-2.51-.968-3.894s.943-2.605 2.07-3.438l1.478-1.094c.334-.245.805-.175 1.05.158s.177.804-.157 1.05l-1.48 1.095c-.803.593-1.326 1.464-1.475 2.45-.148.99.097 1.975.69 2.778 1.225 1.657 3.57 2.01 5.23.785l3.528-2.608c1.658-1.225 2.01-3.57.785-5.23-.498-.674-1.187-1.15-1.992-1.376-.4-.113-.633-.527-.52-.927.112-.4.528-.63.926-.522 1.13.318 2.096.986 2.794 1.932 1.717 2.324 1.224 5.612-1.1 7.33l-3.53 2.608c-.933.693-2.023 1.026-3.105 1.026z"></path></g></svg>
          #{domain}
         </a>
       </p>
    </blockquote>
    """
  end

  defp card_html(%{"card" => card}) do
    Logger.debug("Unsupported card: #{inspect(card)}")
    raise "Unsupported card"
  end

  # no card in tweet
  defp card_html(_tweet), do: ""

  defp poll_option_html(label, count, total) do
    percent = round(count / total * 100.0)

    """
    <strong>#{percent}%</strong>
    #{label}
    <progress max="100" value="#{percent}"></progress>
    """
  end

  defp card_image_html(_card_url, nil, _title), do: ""

  defp card_image_html(card_url, image_url, title) do
    """
    <p>
      <a href="#{card_url}">
        <img src="#{image_url}" alt="#{title}" />
       </a>
    </p>
    """
  end

  defp card_image_url(%{
         "card" => %{
           "binding_values" => %{
             "photo_image_full_size" => %{
               "image_value" => %{"url" => image_url}
             }
           }
         }
       }),
       do: image_url

  defp card_image_url(%{
         "card" => %{
           "binding_values" => %{
             "thumbnail_image_large" => %{
               "image_value" => %{"url" => image_url}
             }
           }
         }
       }),
       do: image_url

  defp card_image_url(_), do: nil

  defp media_html(%{"extended_entities" => %{"media" => media}}) do
    media
    |> Enum.map(&one_media_html/1)
    |> Enum.join("")
  end

  # no media
  defp media_html(_tweet), do: ""

  defp one_media_html(%{"media_url_https" => url, "type" => "photo"}) do
    """
    <p>
      <img src="#{url}" alt="" />
    </p>
    """
  end

  defp one_media_html(%{
         "media_url_https" => image_url,
         "type" => type,
         "video_info" => %{"variants" => variants}
       })
       when type in ["video", "animated_gif"] do
    sources =
      variants
      |> Enum.map(fn %{"content_type" => content_type, "url" => url} ->
        """
        <source src="#{url}" type="#{content_type}">
        """
      end)

    """
    <p>
      <video controls poster="#{image_url}">
        #{sources}
        <img src="#{image_url}" alt="" />
      </video>
    </p>
    """
  end

  def get_guest_token do
    body = HTTP.Client.get_body!("https://twitter.com")

    [guest_token | _] = Regex.run(~r/gt=[0-9]+/m, body)
    String.replace_prefix(guest_token, "gt=", "")
  end

  def get_api_key do
    main_js =
      get_main_js_url()
      |> HTTP.Client.get_body!()

    [api_key | _] = Regex.run(~r/([a-zA-Z0-9]{59}%[a-zA-Z0-9]{44})/m, main_js)
    api_key
  end

  defp get_main_js_url do
    get_document("https://twitter.com")
    |> Meeseeks.all(css("link[href^=\"https://abs.twimg.com/responsive-web/\"]"))
    |> Enum.map(&Meeseeks.attr(&1, "href"))
    |> Enum.find(&is_main_js?/1)
  end

  defp is_main_js?(url) do
    [
      ~r/https:\/\/abs\.twimg\.com\/responsive-web\/web\/main\.[^\.]+\.js/m,
      ~r/https:\/\/abs\.twimg\.com\/responsive-web\/web_legacy\/main\.[^\.]+\.js/m,
      ~r/https:\/\/abs\.twimg\.com\/responsive-web\/client-web\/main\.[^\.]+\.js/m,
      ~r/https:\/\/abs\.twimg\.com\/responsive-web\/client-web-legacy\/main\.[^\.]+\.js/m
    ]
    |> Enum.any?(&Regex.match?(&1, url))
  end

  defp get_document(url) do
    headers = [
      {"user-agent", Http.UserAgents.random_desktop()}
    ]

    url
    |> HTTP.Client.get_body!(headers)
    |> Meeseeks.parse()
  end

  defp get_json(url) do
    headers = [
      {"authorization", "Bearer #{get_api_key()}"},
      {"x-guest-token", get_guest_token()}
    ]

    url
    |> HTTP.Client.get_body!(headers)
    |> Jason.decode!()
  end
end
