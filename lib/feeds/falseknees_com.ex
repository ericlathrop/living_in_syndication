defmodule Feeds.FalsekneesCom do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "False Knees",
        url: "https://falseknees.com"
      }

  import Meeseeks.CSS, only: [css: 1]

  @news_listing_url "https://falseknees.com"
  @comics_to_fetch 10

  @impl RssGenerator
  def get_channel(_params) do
    document = get_document(@news_listing_url)
    open_graph = HTML.OpenGraph.extract_from_html(document)
    title = open_graph["og:site_name"]

    {_delay_seconds, last_page_url} = HTML.meta_refresh(document)
    {last_comic_number, ".html"} = Integer.parse(last_page_url)

    items =
      last_comic_number..(last_comic_number - @comics_to_fetch)
      |> Enum.map(&get_comic/1)

    {:ok,
     %Rss.Channel{
       title: title,
       link: @news_listing_url,
       description: HTML.meta_description(document),
       language: "en-us",
       image: Rss.Image.from_link_rel_icon(document, title, @news_listing_url),
       items: items
     }}
  end

  def get_comic(comic_number) when is_integer(comic_number) and comic_number > 0 do
    title = "Comic ##{comic_number}"
    url = "https://falseknees.com/#{comic_number}.html"

    description =
      get_document(url)
      |> Meeseeks.all(css("div img[src^=\"imgs/#{comic_number}\"]"))
      |> Enum.map(&Meeseeks.attr(&1, "src"))
      |> Enum.map(
        &"""
          <p><img src="#{&1}" alt="#{title}" /></p>
        """
      )
      |> Enum.join("")

    %Rss.Item{
      title: "Comic ##{comic_number}",
      link: url,
      description: description,
      guid: url
    }
  end

  defp get_document(url) do
    url
    |> HTTP.Client.get_body!()
    |> Meeseeks.parse()
  end
end
