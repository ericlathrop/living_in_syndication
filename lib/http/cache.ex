defmodule HTTP.Cache do
  @moduledoc false

  use GenServer

  def start_link(opts) do
    GenServer.start_link(__MODULE__, true, opts)
  end

  @impl true
  def init(true) do
    cache = :ets.new(__MODULE__, [:named_table, :public, read_concurrency: true])
    {:ok, cache}
  end

  def request(method, url, headers, max_age \\ [minutes: -15])
      when is_atom(method) and is_binary(url) and is_list(headers) and is_list(max_age) do
    key = {method, url}
    cutoff_date = Timex.shift(DateTime.utc_now(), max_age)

    case :ets.lookup(__MODULE__, key) do
      [{_key, response, inserted_time}] ->
        if DateTime.compare(inserted_time, cutoff_date) == :lt do
          # too old
          :ets.delete(__MODULE__, key)
          request_and_insert(method, url, headers)
        else
          {:ok, response}
        end

      _ ->
        request_and_insert(method, url, headers)
    end
  end

  defp request_and_insert(method, url, headers) do
    with {:ok, response = %Mojito.Response{status_code: 200}} <-
           Mojito.request(method, url, headers),
         true <- :ets.insert(__MODULE__, {{method, url}, response, DateTime.utc_now()}) do
      {:ok, response}
    else
      {:ok, response} -> {:error, response}
      {:error, reason} -> {:error, reason}
    end
  end
end
