defmodule HTTP.Client do
  @moduledoc false

  def get_body!(url, headers \\ []) when is_binary(url) and is_list(headers) do
    {:ok, %Mojito.Response{status_code: 200, body: body}} = HTTP.Cache.request(:get, url, headers)
    body
  end

  def get_headers!(url) when is_binary(url) do
    {:ok, %Mojito.Response{status_code: 200, headers: headers}} =
      HTTP.Cache.request(:head, url, [])

    headers
  end

  def get_header(headers, key) when is_list(headers) and is_binary(key) do
    Enum.find_value(headers, fn
      {^key, value} -> value
      _x -> nil
    end)
  end
end
