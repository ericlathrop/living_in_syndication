defmodule Rss do
  @moduledoc false

  @enforce_keys [:channel]
  defstruct [
    :channel
  ]

  def to_xml(%Rss{channel: channel}) do
    {:rss, [{:version, "2.0"}, {:"xmlns:atom", "http://www.w3.org/2005/Atom"}],
     [
       Rss.Channel.to_xml(channel)
     ]
     |> Rss.Xml.cleanup_content()}
  end

  def to_xml_string(rss = %Rss{}) do
    xml = Rss.to_xml(rss)

    :xmerl.export_simple([xml], :xmerl_xml)
    |> to_string()
  end
end
