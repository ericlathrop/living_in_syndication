FROM elixir:1.12.3-alpine

RUN apk add --no-cache cargo rust

RUN mkdir /app
WORKDIR /app
COPY ./ /app

ENV MIX_ENV prod

RUN rm -rf /app/deps
RUN mix local.rebar --force
RUN mix local.hex --force
RUN yes | mix deps.get --only prod
RUN mix release

##########################################################

FROM alpine:3.14
MAINTAINER Eric Lathrop <eric@ericlathrop.com>

RUN apk add --no-cache bash curl libgcc libstdc++

RUN mkdir /app
WORKDIR /app
COPY --from=0 /app/_build/prod /app

ENV PORT 80
EXPOSE 80

CMD /app/rel/living_in_syndication/bin/living_in_syndication start
