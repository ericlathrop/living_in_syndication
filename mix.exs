defmodule RssGenerator.MixProject do
  use Mix.Project

  def project do
    [
      app: :living_in_syndication,
      version: "0.3.4",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      package: package(),
      deps: deps(),
      releases: releases(),
      source_url: "https://gitlab.com/ericlathrop/living_in_syndication"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :runtime_tools],
      mod: {LivingInSyndication.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:earmark, "~> 1.4"},
      {:jason, "~> 1.2"},
      {:meeseeks, "~> 0.15"},
      {:mojito, "~> 0.7"},
      {:plug, "~> 1.11"},
      {:plug_cowboy, "~> 2.0"},
      {:timex, "~> 3.6"},
      {:typed_struct, "~> 0.2"}
    ]
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-only"]
    ]
  end

  defp releases do
    [
      living_in_syndication: [
        include_executables_for: [:unix],
        applications: [runtime_tools: :permanent]
      ]
    ]
  end
end
