# Living in Syndication

Generate RSS feeds for sites that don't have them.

![Screenshot showing the Living in Syndication UI](screenshot.png)

Many sites don't provide RSS feeds these days. This project is a self-hosted app
that will scrape the HTML of various sites and create RSS feeds for you. This
app is similar to [RSS-Bridge](https://github.com/RSS-Bridge/rss-bridge), but
supports different sites. My favorite RSS Reader is
[Miniflux](https://github.com/miniflux/v2).

## Supported Sites

* [American Bird Conservancy](https://abcbirds.org)
* [AP Top News](https://apnews.com)
* [eBird](https://ebird.org)
* [False Knees](https://falseknees.com)
* [GitHub Releases](https://github.com)
* [Instagram](https://www.instagram.com)
* [National Audubon Society](https://www.audubon.org)
* [Twitter](https://twitter.com)
* [Twitter (via APIv2)](https://developer.twitter.com/en/docs/twitter-api/early-access)

## Installation

Builds of Living in Syndication are published to [Docker Hub](https://hub.docker.com/r/ericlathrop/living_in_syndication).

You can run a build with Docker like this:

```bash
$ docker run -it -p 4001:80 ericlathrop/living_in_syndication:v0.3.3
```

If you want to make your own build:

1. Clone this repo.
2. [Install Elixir](https://elixir-lang.org/install.html)
3. [Install Rust](https://rustup.rs)
4. Run `mix deps.get` inside the repo.
5. Run `MIX_ENV=prod mix release` inside the repo.
6. The build will be located in the `_build/prod` folder inside the repo. You
   can copy it to a server and run `rel/living_in_syndication/bin/living_in_syndication start` to start the server.

See the [Dockerfile](Dockerfile) for more complete build instructions.

## Help Wanted: Adding More Sites

I'd love PRs for adding more supported sites. Please look at the [eBird
generator](lib/feeds/ebird_org.ex) as an example.
